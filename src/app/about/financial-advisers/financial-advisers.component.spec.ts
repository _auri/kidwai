import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinancialAdvisersComponent } from './financial-advisers.component';

describe('FinancialAdvisersComponent', () => {
  let component: FinancialAdvisersComponent;
  let fixture: ComponentFixture<FinancialAdvisersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinancialAdvisersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinancialAdvisersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
