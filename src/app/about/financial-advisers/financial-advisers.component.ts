import { Component, OnInit } from "@angular/core";
import { FormerSup } from "../superintendents/superintendents.component";

@Component({
  selector: "app-financial-advisers",
  templateUrl: "./financial-advisers.component.html",
  styleUrls: ["./financial-advisers.component.scss"]
})
export class FinancialAdvisersComponent implements OnInit {
  constructor() {}

  formerSup: FormerSup[];
  ngOnInit() {
    this.formerSup = [
      {
        slno: "1",
        from: "20-08-1989",
        to: "15-03-1993",
        name: "Sri. B. Nanjundappa K.S.A.S"
      },
      {
        slno: "2",
        from: " 17-03-1993",
        to: " 02-06-1995",
        name: "Sri. H.L Somshakar K.S.A.S"
      },
      {
        slno: "3",
        from: "03-06-1995",
        to: " 24-06-1995",
        name: "Sri. P. Krishna Reddy Bsc"
      },
      {
        slno: "4",
        from: "25-09-1995",
        to: " 31-07-1996",
        name: "Sri. G.B Suriya Prakash  Bsc, MA, LLB"
      },
      {
        slno: "5",
        from: "01-08-1996",
        to: " 30-04-1998",
        name: "Sri. HN Chandrashakara K.S.A.S"
      },
      {
        slno: "6",
        from: "30-04-1998",
        to: "22-06-1998",
        name: "Sri. . Krishna Reddy Bsc"
      },
      {
        slno: "7",
        from: "22-06-1998",
        to: "10-07-2000",
        name: "Sri. N.B Shivarudrappa  K.S.A.S"
      },
      {
        slno: "8",
        from: "26-07-2000",
        to: " 01-03-2004",
        name: "Sri. H.L Somshakar K.S.A.S"
      },
      {
        slno: "9",
        from: "22-11-2004",
        to: "13-07-2005",
        name: "Sri.Chikkammadeva  K.S.A.S"
      },
      {
        slno: "10",
        from: "19-07-2005",
        to: "31-03-2008",
        name: "Sri. K.R Ramamurthy K.S.A.S"
      },
      {
        slno: "11",
        from: " 01-04-2008",
        to: "31-12-2008",
        name: "Sri. Subhash B Tavagha"
      },
      {
        slno: "12",
        from: "01-01-2009",
        to: "31-07-2014",
        name: "Srimate. Nirmala K.S.A.S"
      }
    ];
  }
}
