import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-phone',
  templateUrl: './phone.component.html',
  styleUrls: ['./phone.component.scss']
})
export class PhoneComponent implements OnInit {

  constructor() { }
  phone: any[] = [{
    "slno":"1",
    "departments":"DIRECTOR PA",
    "room":"44",
    "phone":"08026094002"
},
{
    "slno":"2",
    "departments":"DIRECTOR OFFICE RECEPTION ",
    "room":"44",
    "phone":"08026094004"
},
{
    "slno":"3",
    "departments":"BLOOD BANK ",
    "room":"112",
    "phone":"08026094082"
},{
    "slno":"4",
    "departments":"MEDICAL SUPERINTENDENT",
    "room":"43",
    "phone":"08026094003"
},{
    "slno":"5",
    "departments":"CHIEF ADMINISTRATIVE OFFICER  ",
    "room":"47",
    "phone":"08026094005"
},{
    "slno":"6",
    "departments":"FINANCIAL ADVISER OFFICE  ",
    "room":"48",
    "phone":"08026094006"
},{
    "slno":"7",
    "departments":"RESIDENT MEDICAL OFFICER OFFICE ",
    "room":"48",
    "phone":"08026094007"
},{
    "slno":"8",
    "departments":"RESIDENT MEDICAL OFFICER OFFICE ",
    "room":"48",
    "phone":"08026561556"
},{
    "slno":"9",
    "departments":"BOARD NUMBER ",
    "room":"0",
    "phone":"08026094000"
},{
    "slno":"10",
    "departments":"SECURITY ROOM ",
    "room":"0",
    "phone":"08026094051"
},{
    "slno":"11",
    "departments":"A.K WARD ",
    "room":"115",
    "phone":"08026094067"
},{
    "slno":"12",
    "departments":"ACADEMIC CELL ",
    "room":"0",
    "phone":"08026094024"
},{
    "slno":"13",
    "departments":"ACCOUNT SECTION  ",
    "room":"51",
    "phone":"08026094010"
},{
    "slno":"14",
    "departments":"ADMINISTRATIVE OFFICER  ",
    "room":"49",
    "phone":"08026094013"
},{
    "slno":"15",
    "departments":"ADMINISTRATIVE OFFICER  ",
    "room":"49",
    "phone":"08026094015"
},{
    "slno":"16",
    "departments":"ANAESTHESIA HOD ",
    "room":"106",
    "phone":"08026094089"
},{
    "slno":"17",
    "departments":"ANIKETHANA WARD 1ST FLOOR  ",
    "room":"206",
    "phone":"08026094128"
},{
    "slno":"18",
    "departments":"ANIKETHANA WARD 2ND FLOOR  ",
    "room":"0",
    "phone":"08026094130"
},{
    "slno":"19",
    "departments":"ANIKETHANA WARD 3RD FLOOR  ",
    "room":"212",
    "phone":"08026094131"
},{
    "slno":"20",
    "departments":"ANIKETHANA WARD 4TH FLOOR  ",
    "room":"0",
    "phone":"08026094134"
},{
    "slno":"21",
    "departments":"ANIKETHANA WARD GROUND FLOOR  ",
    "room":"203",
    "phone":"08026094129"
},{
    "slno":"22",
    "departments":"ASHWINI WARD  ",
    "room":"117",
    "phone":"08026094069"
},{
    "slno":"23",
    "departments":"AYUSHA WARD  ",
    "room":"505",
    "phone":"08026094107"
},{
    "slno":"24",
    "departments":"BARC ",
    "room":"0",
    "phone":"08026094054"
},{
    "slno":"25",
    "departments":"BIBI AYESHA WARD  ",
    "room":"505",
    "phone":"08026094106"
},{
    "slno":"26",
    "departments":"BIOCHEMISTRY DEPARTMENT  ",
    "room":"113",
    "phone":"08026094072"
},{
    "slno":"27",
    "departments":"BIOCHEMISTRY HOD  ",
    "room":"113",
    "phone":"08026094071"
},{
    "slno":"28",
    "departments":"BIOSTATISTICS DEPARTMENT  ",
    "room":"55",
    "phone":"08026094012"
},{
    "slno":"29",
    "departments":"BIOSTATISTICS DEPARTMENT  ",
    "room":"55",
    "phone":"08026094014"
},{
    "slno":"30",
    "departments":"BIOSTATISTICS DEPARTMENT  ",
    "room":"55",
    "phone":"08026094022"
},{
    "slno":"31",
    "departments":"BLOOD BANK  ",
    "room":"112",
    "phone":"08026094083"
},{
    "slno":"32",
    "departments":"BLOOD COLLECTION  ",
    "room":"2",
    "phone":"08026094034"
},{
    "slno":"33",
    "departments":"BRACHYTHERAPHY (RADIATION ONCOLOGY) ",
    "room":"33",
    "phone":"08026094042"
},{
    "slno":"34",
    "departments":"CASH COUNTER (ANIKETANA) ",
    "room":"206",
    "phone":"08026094125"
},{
    "slno":"35",
    "departments":"CELL BIOLOGY (PATHOLOGY) ",
    "room":"120 B",
    "phone":"08026094078"
},{
    "slno":"36",
    "departments":"CENTRAL MEDICAL STORE  ",
    "room":"0",
    "phone":"08026094052"
},{
    "slno":"37",
    "departments":"COMPUTER ROOM  ",
    "room":"50",
    "phone":"08026094011"
},{
    "slno":"38",
    "departments":"CURIE WARD  ",
    "room":"103",
    "phone":"08026094064"
},{
    "slno":"39",
    "departments":"CYTOLOGY LAB  ",
    "room":"120 A",
    "phone":"08026094079"
},{
    "slno":"40",
    "departments":"DAY CARE  ",
    "room":"102",
    "phone":"08026094065"
},{
    "slno":"41",
    "departments":"DAY CARE WARD  ",
    "room":"102",
    "phone":"08026094055"
},{
    "slno":"42",
    "departments":"DHARMASALA  ",
    "room":"0",
    "phone":"08026094094"
},{
    "slno":"43",
    "departments":"DIRECTOR CHAMBER  ",
    "room":"44",
    "phone":"08026094112"
},{
    "slno":"44",
    "departments":"DIRECTOR QUATRES  ",
    "room":"0",
    "phone":"08026094111"
},{
    "slno":"45",
    "departments":"DISPATCH ",
    "room":"49",
    "phone":"08026094016"
},{
    "slno":"46",
    "departments":"DRIVER SECTION  ",
    "room":"0",
    "phone":"08026094102"
},{
    "slno":"47",
    "departments":"ELECTRICAL DEPARTMENT  ",
    "room":"0",
    "phone":"08026094053"
},{
    "slno":"48",
    "departments":"ENDOSCOPY (SURGICAL) ",
    "room":"68",
    "phone":"08026094086"
},{
    "slno":"49",
    "departments":"ESI COUNTER ",
    "room":"16",
    "phone":"08026094119"
},{
    "slno":"50",
    "departments":"FEMAL DUTY ROOM  ",
    "room":"0",
    "phone":"08026094058"
},{
    "slno":"51",
    "departments":"GYNAECOLOGY OPD  ",
    "room":"24",
    "phone":"08026094039"
},{
    "slno":"52",
    "departments":"HEAD AND NECK MINOR OT  ",
    "room":"73",
    "phone":"08026094031"
},{
    "slno":"53",
    "departments":"HEAD AND NECK OPD  ",
    "room":"59",
    "phone":"08026094037"
},{
    "slno":"54",
    "departments":"HEMATALOGY LAB ",
    "room":"121",
    "phone":"08026094044"
},{
    "slno":"55",
    "departments":"HISTOPATHOLOGY LAB ",
    "room":"119",
    "phone":"08026094080"
},{
    "slno":"56",
    "departments":"JINDAL WARD  ",
    "room":"303",
    "phone":"08026094087"
},{
    "slno":"57",
    "departments":"K C D F  ",
    "room":"0",
    "phone":"08026094048"
},{
    "slno":"58",
    "departments":"KAPUR WARD  ",
    "room":"0",
    "phone":"08026094097"
},{
    "slno":"59",
    "departments":"KARUNYA WARD  ",
    "room":"77",
    "phone":"08026094076"
},{
    "slno":"60",
    "departments":"KITCHEN  ",
    "room":"0",
    "phone":"08026094093"
},{
    "slno":"61",
    "departments":"LIABRARY II ",
    "room":"0",
    "phone":"08026094109"
},{
    "slno":"62",
    "departments":"MAJOR OT  ",
    "room":"105",
    "phone":"08026094061"
},{
    "slno":"63",
    "departments":"MALE DUTY ROOM  ",
    "room":"0",
    "phone":"08026094056"
},{
    "slno":"64",
    "departments":"MANJUSHREE WARD  ",
    "room":"78",
    "phone":"08026094059"
},{
    "slno":"65",
    "departments":"MEDICAL DEPARTMENT OPD  ",
    "room":"18",
    "phone":"08026094035"
},{
    "slno":"66",
    "departments":"MEDICAL ICU  ",
    "room":"301",
    "phone":"08026094090"
},{
    "slno":"67",
    "departments":"MEDICAL RECORD DEPARTMENT  ",
    "room":"14",
    "phone":"08026094066"
},{
    "slno":"68",
    "departments":"MEDICAL RECORD DEPARTMENT II ",
    "room":"57",
    "phone":"08026094019"
},{
    "slno":"69",
    "departments":"MEJOR OT PROFESSOR ROOM  ",
    "room":"105",
    "phone":"08026094026"
},{
    "slno":"70",
    "departments":"MICROBILOGY LAB  ",
    "room":"114",
    "phone":"08026094074"
},{
    "slno":"71",
    "departments":"MICROBIOLOGY HOD  ",
    "room":"114",
    "phone":"08026094073"
},{
    "slno":"72",
    "departments":"NUCLEAR MEDICIAN  ",
    "room":"54",
    "phone":"08026094020"
},{
    "slno":"73",
    "departments":"NURSING COLLEGE  ",
    "room":"0",
    "phone":"08026094098"
},{
    "slno":"74",
    "departments":"NURSING COLLEGE (PRINCIPAL) ",
    "room":"0",
    "phone":"08026094110"
},{
    "slno":"75",
    "departments":"NURSING SUPERINTENDENT  ",
    "room":"81",
    "phone":"08026094127"
},{
    "slno":"76",
    "departments":"NURSING SUPERINTENDENT OFFICE  ",
    "room":"46",
    "phone":"08026094008"
},{
    "slno":"77",
    "departments":"ORAL OPD ",
    "room":"70",
    "phone":"08026094038"
},{
    "slno":"78",
    "departments":"OT  ",
    "room":"105",
    "phone":"08026094062"
},{
    "slno":"79",
    "departments":"OT-ICU ",
    "room":"106",
    "phone":"08026094063"
},{
    "slno":"80",
    "departments":"P G HOSTEL (WOMEN) ",
    "room":"0",
    "phone":"08026094116"
},{
    "slno":"81",
    "departments":"P R C WARD  ",
    "room":"0",
    "phone":"08026094100"
},{
    "slno":"82",
    "departments":"P W D  ",
    "room":"0",
    "phone":"08026566075"
},{
    "slno":"83",
    "departments":"P W D  ",
    "room":"0",
    "phone":"08026094075"
},{
    "slno":"84",
    "departments":"P.R.O OFFICE  ",
    "room":"13",
    "phone":"08026094032"
},{
    "slno":"85",
    "departments":"PATHOLOGY DEPARTMENT ",
    "room":"119",
    "phone":"08026094081"
},{
    "slno":"86",
    "departments":"PEADIATRIC OPD  ",
    "room":"0",
    "phone":"08026094046"
},{
    "slno":"87",
    "departments":"PHOTOGRAPHY  ",
    "room":"118",
    "phone":"08026094070"
},{
    "slno":"88",
    "departments":"PREANAESTHESIA CHECKUP ",
    "room":"71",
    "phone":"08026094091"
},{
    "slno":"89",
    "departments":"RADIATION ONCOLOGY  ",
    "room":"125",
    "phone":"08026094084"
},{
    "slno":"90",
    "departments":"RADIATION PHYSICS  ",
    "room":"37",
    "phone":"08026094050"
},{
    "slno":"91",
    "departments":"RADIATION PHYSICS  ",
    "room":"37",
    "phone":"08026094043"
},{
    "slno":"92",
    "departments":"RADIATION PHYSICS HOD ",
    "room":"0",
    "phone":"08026094028"
},{
    "slno":"93",
    "departments":"RADIODIAGNOSIS DEPARTENT  ",
    "room":"9",
    "phone":"08026094045"
},{
    "slno":"94",
    "departments":"RADIODIAGNOSIS DEPARTENT  ",
    "room":"42",
    "phone":"08026094017"
},{
    "slno":"95",
    "departments":"RADIODIAGNOSIS DEPARTENT (MRI) ",
    "room":"4",
    "phone":"08026094029"
},{
    "slno":"96",
    "departments":"RADIODIOGNOSIS (X RAY REPORT) ",
    "room":"53",
    "phone":"08026094023"
},{
    "slno":"97",
    "departments":"RADIOTHERAPHY  ",
    "room":"39",
    "phone":"08026094049"
},{
    "slno":"98",
    "departments":"RADIOTHERAPHY DEPARTMENT   ",
    "room":"32",
    "phone":"08026094041"
},{
    "slno":"99",
    "departments":"RADIOTHERAPHY DEPARTMENT   ",
    "room":"125",
    "phone":"08026094085"
},{
    "slno":"100",
    "departments":"RADIOTHERAPHY OPD ",
    "room":"28",
    "phone":"08026094040"
},{
    "slno":"101",
    "departments":"ROENTOGEN WARD  ",
    "room":"122 A",
    "phone":"080260940018"
},{
    "slno":"102",
    "departments":"SANTHIDHAMA GROUND FLOOR  ",
    "room":"501",
    "phone":"08026094104"
},{
    "slno":"103",
    "departments":"SEMINAR HALL ",
    "room":"0",
    "phone":"08026094027"
},{
    "slno":"104",
    "departments":"SHANTIDHAMA 1ST FLOOR  ",
    "room":"502",
    "phone":"08026094105"
},{
    "slno":"105",
    "departments":"SOCAIL WELPARE OFFICE  ",
    "room":"62",
    "phone":"08026094095"
},{
    "slno":"106",
    "departments":"SOWKYA WARD  ",
    "room":"76",
    "phone":"08026094068"
},{
    "slno":"107",
    "departments":"STEP ONE ICU  ",
    "room":"106",
    "phone":"08026094060"
},{
    "slno":"108",
    "departments":"STOMA CLINIC  ",
    "room":"0",
    "phone":"08026094030"
},{
    "slno":"109",
    "departments":"SUPLAY SECTION  ",
    "room":"52",
    "phone":"08026094009"
},{
    "slno":"110",
    "departments":"SURGICAL OPD  ",
    "room":"63",
    "phone":"08026094036"
},{
    "slno":"111",
    "departments":"TALLY SECTION  ",
    "room":"51",
    "phone":"08026094092"
},{
    "slno":"112",
    "departments":"TELEPHONE EXCHANGE  ",
    "room":"0",
    "phone":"08026094077"
},{
    "slno":"113",
    "departments":"TREATMENT ROOM (EMERGENCY) ",
    "room":"109",
    "phone":"08026094057"
},{
    "slno":"114",
    "departments":"VAJAPAYEE CLAIME SECTION  ",
    "room":"58",
    "phone":"08026094123"
},{
    "slno":"115",
    "departments":"VAJAPAYEE GROUND FLOOR  ",
    "room":"23",
    "phone":"08026094096"
},{
    "slno":"116",
    "departments":"VATHSALYA WARD  ",
    "room":"80",
    "phone":"08026094135"
},{
    "slno":"117",
    "departments":"VISHALYA WARD  ",
    "room":"79",
    "phone":"08026094126"
}];
  ngOnInit() {
  }

}
