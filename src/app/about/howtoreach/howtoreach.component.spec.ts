import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HowtoreachComponent } from './howtoreach.component';

describe('HowtoreachComponent', () => {
  let component: HowtoreachComponent;
  let fixture: ComponentFixture<HowtoreachComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HowtoreachComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HowtoreachComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
