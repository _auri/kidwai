import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommittessComponent } from './committess.component';

describe('CommittessComponent', () => {
  let component: CommittessComponent;
  let fixture: ComponentFixture<CommittessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommittessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommittessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
