import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-committess",
  templateUrl: "./committess.component.html",
  styleUrls: ["./committess.component.scss"]
})
export class CommittessComponent implements OnInit {
  constructor() {}
  Council;
  purch;
  Equipment;
  Drugs;
  Diet;
  linen;
  hospital;
  ngOnInit() {
    this.hospital = [
      {
        column: "Chairman",
        name:
          "Professor and Head, Department of Radiology Kidwai Memorial Institute of Oncology, Bangalore- 560029",
        slno: "1"
      },
      {
        column: "Member",
        name:
          "Assistant Professor, Department of Surgical Oncology Kidwai Memorial Institute of Oncology, Bangalore- 560029",
        slno: "2"
      },
      {
        column: "Member",
        name:
          "Financial Adviser Kidwai Memorial Institute of Oncology,Bangalore- 560029",
        slno: "3"
      },
      {
        column: "Member",
        name:
          "Nursing Superintendent - Grade 1 Kidwai Memorial Institute of Oncology,Bangalore- 560029",
        slno: "4"
      },
      {
        column: "Member/Secretary",
        name:
          "Resident Medical Officer Kidwai Memorial Institute of Oncology, Bangalore- 560029",
        slno: "5"
      }
    ];
    this.linen = [
      {
        column: "Chairman",
        name:
          "Professor and Head, Department of Surgical Oncology Kidwai Memorial Institute of Oncology,Bangalore- 560029",
        slno: "1"
      },
      {
        column: "Member",
        name:
          "Chief Administrative Officer Kidwai Memorial Institute of Oncology, Bangalore- 560029",
        slno: "2"
      },
      {
        column: "Member",
        name:
          "Financial Adviser Kidwai Memorial Institute of Oncology,Bangalore- 560029",
        slno: "3"
      },
      {
        column: "Member",
        name:
          "Assistant Professor, Department of Biochemistry Kidwai Memorial Institute of Oncology,Bangalore- 560029",
        slno: "4"
      },
      {
        column: "Member",
        name:
          "Resident Medical Officer Kidwai Memorial Institute of Oncology, Bangalore- 560029",
        slno: "5"
      },
      {
        column: "Member/Secretary",
        name:
          "Nursing Superintendent - Grade 1 Kidwai Memorial Institute of Oncology, Bangalore- 560029",
        slno: "6"
      }
    ];
    this.Diet = [
      {
        column: "Chairman",
        name:
          "Professor and Head, Department of Gynaecological Oncology Kidwai Memorial Institute of Oncology, Bangalore- 560029",
        slno: "1"
      },
      {
        column: "Member",
        name:
          "Assistant Professor, Department of Microbiology Kidwai Memorial Institute of Oncology, Bangalore- 560029",
        slno: "2"
      },
      {
        column: "Member",
        name:
          "Chief Administrative Officer Kidwai Memorial Institute of Oncology, Bangalore- 560029",
        slno: "3"
      },
      {
        column: "Member",
        name:
          "Nursing Superintendent - Grade 1 Kidwai Memorial Institute of Oncology,Bangalore- 560029",
        slno: "4"
      },
      {
        column: "Member/Secretary",
        name:
          "Dietitian Kidwai Memorial Institute of Oncology, Bangalore- 560029",
        slno: "5"
      }
    ];
    this.Drugs = [
      {
        column: "Chairman",
        name:
          "Professor and Head, Department of Medical Oncology Kidwai Memorial Institute of Oncology, Bangalore- 560029",
        slno: "1"
      },
      {
        column: "Member",
        name:
          "Professor and Head, Department of Surgical Oncology Kidwai Memorial Institute of Oncology, Bangalore- 560029",
        slno: "2"
      },
      {
        column: "Member",
        name:
          "Professor and Head, Department of Radiology Kidwai Memorial Institute of Oncology, Bangalore- 560029",
        slno: "3"
      },
      {
        column: "Member",
        name:
          "Professor and Head, Department of Gynaecology Oncology Kidwai Memorial Institute of Oncology, Bangalore- 560029",
        slno: "4"
      },
      {
        column: "Member/Secretary",
        name:
          "Resident Medical Officer Kidwai Memorial Institute of Oncology, Bangalore- 560029",
        slno: "5"
      }
    ];
    this.Equipment = [
      {
        column: "Chairman",
        name:
          "Medical Superintendent Kidwai Memorial Institute of Oncology,Bangalore- 560029",
        slno: "1"
      },
      {
        column: "Member",
        name:
          "Heads of Departments / Units Kidwai Memorial Institute of Oncology,Bangalore- 560029",
        slno: "2"
      },
      {
        column: "Member",
        name:
          "Financial Adviser Kidwai Memorial Institute of Oncology,Bangalore- 560029",
        slno: "3"
      },
      {
        column: "Member",
        name:
          "Nursing Superintendent (Grade 1) Kidwai Memorial Institute of Oncology, Bangalore- 560029",
        slno: "4"
      },
      {
        column: "Member/Secretary",
        name:
          "Resident Medical Officer Kidwai Memorial Institute of Oncology, Bangalore- 560029",
        slno: "5"
      }
    ];
    this.purch = [
      {
        column: "Chairman",
        name:
          "Director Kidwai Memorial Institute of Oncology, Bangalore- 560029",
        slno: "1"
      },
      {
        column: "Member",
        name:
          "Professor & HOD, Department of Surgical Oncology Kidwai Memorial Institute of Oncology, Bangalore- 560029",
        slno: "2"
      },
      {
        column: "Member",
        name:
          "Professor & HOD, Department of Radiation Oncology Kidwai Memorial Institute of Oncology,Bangalore- 560029",
        slno: "3"
      },
      {
        column: "Member",
        name:
          "Professor & HOD, Department of Medical Oncology Kidwai Memorial Institute of Oncology,Bangalore- 560029",
        slno: "4"
      },
      {
        column: "Member",
        name:
          "Professor & HOD, Department of Pathology Kidwai Memorial Institute of Oncology, Bangalore- 560029",
        slno: "5"
      },
      {
        column: "Member",
        name:
          "Finance Adviser Kidwai Memorial Institute of Oncology, Bangalore- 560029",
        slno: "6"
      },
      {
        column: "Member/Secretary",
        name:
          "Chief Administrative Officer Kidwai Memorial Institute of Oncology, Bangalore- 560029",
        slno: "7"
      }
    ];
    this.Council = [
      {
        column: "Chairman",
        name:
          "Honourable Minister for Medical Education Governing of Karnataka Vidhana Soudha, Bengaluru - 560001",
        slno: "1"
      },
      {
        column: "Member",
        name:
          "Additional Chief Secretary to the Government of Karnataka Finance Department Vidhana Soudha, Bangalore- 560001",
        slno: "2"
      },
      {
        column: "Member",
        name:
          "Principal Secretary to the Government of Karnataka Department of Health & Family Welfare Services Vikasa Soudha, Bangalore -560001",
        slno: "3"
      },
      {
        column: "Member",
        name:
          "Principal Secretary to Government of Karnataka Department of Medical Education Vidhana Soudha, Bangalore - 560001",
        slno: "4"
      },
      {
        column: "Member",
        name:
          "Principal Secretary to Government of Karnataka Department of Science & Information Technology M.S.Building, Bangalore -560 001",
        slno: "5"
      },
      {
        column: "Member",
        name:
          "Vice Chancellor Rajiv Gandhi University of Health Sciences (RGUHS) Jayanagar IV Block, Bangalore- 560046",
        slno: "6"
      },
      {
        column: "Member",
        name:
          "Director of Medical Education Government of Karnataka Anand Rao Circle, Bangalore - 560009",
        slno: "7"
      },
      {
        column: "Member",
        name:
          "Director of Health & Family Welfare Services Government of Karnataka Anand Rao Circle, Bangalore – 560009",
        slno: "8"
      },
      {
        column: "Member",
        name:
          "Honourable Minister for Medical Education Governing of Karnataka",
        slno: "9"
      },
      {
        column: "Member",
        name:
          "The Director – General of Health Services Ministry of Health & Family Welfare Government of India Nirman Bhavan, New Delhi – 110011",
        slno: "10"
      },
      {
        column: "Member",
        name:
          "The Chief Executive Board of Radiation & Isotope Technology V. N. Purav Marg, Deonar, Mumbai",
        slno: "11"
      },
      {
        column: "Member",
        name:
          "The Director-General Indian Council of Medical Research Ansari Nagar, New Delhi- 110029",
        slno: "12"
      },
      {
        column: "Member",
        name:
          "Sri Puttanna Member of Legislative Council Karnataka Legislature,No:58, 5th Cross,  III Main, Basaveshwara Layout, Vijayanagar Bangalore- 560040.          ",
        slno: "13"
      },
      {
        column: "Member",
        name:
          "Sri B L S Murthy Chairman, Dharamshala Committee No:144, V Main Road, Chamarajpet Bangalore- 560018",
        slno: "14"
      },
      {
        column: "Member",
        name:
          "Sri Dr.Shanthaveera Mahaswamiji Chairman, Board of Visitors, KMIO,  Kolada Mutt Mahasamsthana,	H.Siddaiah Road, Bangalore- 560027",
        slno: "15"
      },
      {
        column: "Member",
        name:
          "Sri B.C.Sadashivaiah Member No.70, 17th E Main, 6th Phase, Koramangala, Bangalore-560095",
        slno: "16"
      },
      {
        column: "Member",
        name:
          "Smt.Sevanthi M.Hegde, No:27, 4th Main, 7th Cross, BSK 1st Stage, Srinivasa Nagar, Bangalore-560005",
        slno: "17"
      },
      {
        column: "Member",
        name:
          "Dr. Srimani Rajagopalan,No:301, Tharani, 4th Phase, 7th Block, 3rd Stage, Banashankari,	Bangalore- 560085",
        slno: "18"
      },
      {
        column: "Member",
        name:
          "Sri N.L.Gangadhar No:7A, 18th Main Road, Vittal Nagar, Kumaraswamy Layout,Bangalore- 560078",
        slno: "19"
      },
      {
        column: "Member",
        name:
          "Smt.N.Savitha No:18 & 18, 1st Main Road,  Bhavani Layout, Banashankari, Bangalore- 560085",
        slno: "20"
      },
      {
        column: "Member",
        name:
          "Medical Superintendent Kidwai Memorial Institute of Oncology Bangalore- 560029",
        slno: "21"
      },
      {
        column: "Member",
        name:
          "Chief Administrative Officer Kidwai Memorial Institute of Oncology Bangalore- 560029",
        slno: "22"
      },
      {
        column: "Member",
        name:
          "Financial Adviser Kidwai Memorial Institute of Oncology Bangalore- 560029",
        slno: "23"
      },
      {
        column: "Member/Secretary",
        name:
          "Director Kidwai Memorial Institute of Oncology,Governing Council,Bangalore- 560029",
        slno: "24"
      }
    ];
  }
}
