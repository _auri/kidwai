import { Component, OnInit } from "@angular/core";
export interface HistoryEvents {
  year;
  events;
}
@Component({
  selector: "app-history",
  templateUrl: "./history.component.html",
  styleUrls: ["./history.component.scss"]
})
export class HistoryComponent implements OnInit {
  events: HistoryEvents[];
  src: any;
  constructor() {}

  ngOnInit() {
    this.events = [
      {
        year: "1957",
        events: "Conception of the Hospital by the City forefathers"
      },
      {
        year: "1963",
        events:
          "Foundation Stone laid at Parade Ground near Mahatma Gandhi Road which was subsequently transferred to the present location on Hosur Road Bangalore."
      },
      {
        year: "1970",
        events:
          "Taking over of the Hospital by the Karnataka Government vide G.O. No. HMA 84 MDA 68, dated 19thFevruary 1970 as there was no progress of work"
      },
      {
        year: "1971",
        events:
          "Creation and sanction of posts vide G.O. No. HMA 213 MEN 70, dated 21st October 1971."
      },
      {
        year: "1972",
        events:
          "Construction of the first phase of the building 13th December 1972."
      },
      {
        year: "1973",
        events:
          "Establishment of the Radio Diagnostic and Radiation Therapy Department with out- patient block: for other specialty needs with visiting Professors from the Bangalore Medical College, Bangalore: 50 in -patient beds were also provided"
      },
      {
        year: "1975",
        events:
          "The second phase of the development—the functioning of a full-fledged Cancer Institute with fully organized and manned departments was achieved through Construction of the main in-patient block. Accommodation for the out-door clinics: administrative wing, Operation Theatre Complex and ward wings. The inpatient block was commissioned with 150 beds In December 1975"
      },
      {
        year: "1975",
        events:
          "The second phase of the development—the functioning of a full-fledged Cancer Institute with fully organized and manned departments was achieved through Construction of the main in-patient block. Accommodation for the out-door clinics: administrative wing, Operation Theatre Complex and ward wings. The inpatient block was commissioned with 150 beds In December 1975"
      },
      {
        year: "1976",
        events: "Bed-strength increased to 160"
      },
      {
        year: "1977",
        events: "Bed-strength increased to 180"
      },
      {
        year: "1978",
        events: "Bed-strength further increased to 190; Completion of Kitchen Block during March 1978 and Laundry in June 1978 and Mortuary during November 1978"
      },
      {
        year: "1979",
        events: "Completion of Residential Blocks during April, 1979: Remodeling (air-conditioning) of the Operation theatre, Central Gas and Oxygen Supply Systems and Central Suction of O.T. complex. Remodeling of the laboratories (Pathology, Microbiology, Biochemistry, and Cytogenetic)."
      }
    ];
  }
  imgSrc(src){
    this.src = src;
  }
}
