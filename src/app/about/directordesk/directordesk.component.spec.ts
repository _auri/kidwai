import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DirectordeskComponent } from './directordesk.component';

describe('DirectordeskComponent', () => {
  let component: DirectordeskComponent;
  let fixture: ComponentFixture<DirectordeskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DirectordeskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DirectordeskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
