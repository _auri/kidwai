import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormerdeansComponent } from './formerdeans.component';

describe('FormerdeansComponent', () => {
  let component: FormerdeansComponent;
  let fixture: ComponentFixture<FormerdeansComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormerdeansComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormerdeansComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
