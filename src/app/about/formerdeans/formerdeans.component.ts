import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-formerdeans",
  templateUrl: "./formerdeans.component.html",
  styleUrls: ["./formerdeans.component.scss"]
})
export class FormerdeansComponent implements OnInit {
  formerSup: { slno: string; from: string; to: string; name: string }[];

  constructor() {}

  ngOnInit() {
    this.formerSup = [
      {
        from: "05-02-1972",
        name: " Dr. V. RAMACHANDRA NAIDU, M.B.B.S, DMRT.",
        slno: "1",
        to: "31-12-1973"
      },
      {
        from: "01-01-1974",
        name: "Dr. M. APPAJEE, M.B.B.S, Ph.D.",
        slno: "2",
        to: "01-03-1974"
      },
      {
        from: "02-03-1974",
        name: " Dr. K. V. GHORPADE, M.B.B.S, Ph.D, F.R.C PATH (LOND).",
        slno: "3",
        to: "31-03-1974"
      },
      {
        from: "01-04-1974",
        name: "Dr. M. APPAJEE, M.B.B.S, Ph.D",
        slno: "4",
        to: "07-06-1974"
      },
      {
        from: "08-06-1974",
        name: "Dr. G. S. MALLIKARJUNAIAH, M.B.B.S, M.D.",
        slno: "5",
        to: "17-07-1974"
      },
      {
        from: "18-07-1974",
        name: "Dr. A. J. NARENDRAN, B.Sc, M.B.B.S, F.R.C.S.",
        slno: "6",
        to: "30-04-1976"
      },
      {
        from: "01-05-1976",
        name: "Dr. M. KRISHNA BHARGAVA, M.B.B.S, M.D.",
        slno: "7",
        to: "22-01-1980"
      }
    ];
  }
}
