import { Component, OnInit } from "@angular/core";
import { from } from "rxjs";

export interface FormerSup {
  slno: any;
  name: any;
  from: any;
  to: any;
}

@Component({
  selector: "app-superintendents",
  templateUrl: "./superintendents.component.html",
  styleUrls: ["./superintendents.component.scss"]
})
export class SuperintendentsComponent implements OnInit {
  constructor() {}
  formerSup: FormerSup[];
  ngOnInit() {
    this.formerSup = [
      {
        from: "12-12-1980",
        name: " Dr. A.J Narendran B.Sc, M.B.B.S, PECS",
        slno: "1",
        to: "02-07-1982"
      },
      {
        from: "03-07-1982",
        name: "Dr. N Ananth  M.D, DMRS",
        slno: "2",
        to: "24-05-1990"
      },
      {
        from: "24-05-1990",
        name: "Dr. N Lalitha M.D",
        slno: "3",
        to: "30-06-1994"
      },
      {
        from: "23-12-1994",
        name: "Dr. Hema SriradharB.Sc, M.D",
        slno: "4",
        to: "06-08-2003"
      },
      {
        from: "07-08-2003",
        name: "Dr. P.P Bapsy M.D, D.M",
        slno: "5",
        to: "03-04-2007"
      },
      {
        from: "04-04-2007",
        name: "Dr. Ashok M Shenoy MS, FRCS",
        slno: "6",
        to: "06-09-2008"
      },
      {
        from: "06-09-2008",
        name: "Dr. L Appaji M.D, JDM",
        slno: "7",
        to: "31-03-2015"
      },
      {
        from: "13-07-2015",
        name: "Dr. K.P.R Pramod M.D,DNB",
        slno: "8",
        to: "31-08-2016"
      },
      {
        from: "26-11-2016",
        name: "Dr. U.D Bafna M.D",
        slno: "9",
        to: "08-03-2017"
      },
      {
        from: "08-03-2017",
        name: "Dr. S. Krishnamurthy M.ch",
        slno: "10",
        to: "31-01-2018"
      },
      {
        from: "15-03-2018",
        name: "Dr. Sheela V. kumar M.D",
        slno: "11",
        to: "28-02-2019"
      }
    ];
  }
}
