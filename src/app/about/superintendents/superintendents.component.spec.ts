import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperintendentsComponent } from './superintendents.component';

describe('SuperintendentsComponent', () => {
  let component: SuperintendentsComponent;
  let fixture: ComponentFixture<SuperintendentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuperintendentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperintendentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
