import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperintendentdeskComponent } from './superintendentdesk.component';

describe('SuperintendentdeskComponent', () => {
  let component: SuperintendentdeskComponent;
  let fixture: ComponentFixture<SuperintendentdeskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuperintendentdeskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperintendentdeskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
