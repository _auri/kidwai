import { Component, OnInit } from "@angular/core";
import { FormerSup } from "../superintendents/superintendents.component";

@Component({
  selector: "app-formerdirector",
  templateUrl: "./formerdirector.component.html",
  styleUrls: ["./formerdirector.component.scss"]
})
export class FormerdirectorComponent implements OnInit {
  constructor() {}
  formerSup: FormerSup[];
  ngOnInit() {
    this.formerSup = [
      {
        from: "23-01-1980",
        name: "Dr. M. KRISHNA BHARGAVA, M.B.B.S, M.D.",
        slno: "1",
        to: "20-09-1982"
      },
      {
        from: "20-09-1982",
        name: "Dr. M. M. THAHIR, M.B.B.S, M.S, D.A.R.C.P (LOND) R.C.S (ENG.)",
        slno: "2",
        to: "31-03-1983"
      },
      {
        from: "31-03-1983",
        name: "Dr. M. KRISHNA, BHARGAVA M.B.B.S, M.D.",
        slno: "3",
        to: "31-03-1990"
      },
      {
        from: "01-04-1990",
        name: "Dr. N. ANANTHA, M.D, D.M.R.E.",
        slno: "4",
        to: "31-08-1996"
      },
      {
        from: "01-01-1992",
        name: "Dr. P. S. PRABHAKARAN, MBBS, MS.",
        slno: "5",
        to: "31-05-2005"
      },
      {
        from: "01-06-2005",
        name: "Dr. P. P. BAPSY,  I/C,MD, DM.",
        slno: "6",
        to: "30-09-2007"
      },
      {
        from: "01-10-2007",
        name: "Dr. VIJAYALAXMI DESHMANE, I/C, M.B.B.S, M.S.",
        slno: "7",
        to: "11-02-2008"
      },
      {
        from: "12-02-2008",
        name: "Dr. RAMANANDA SHETTY,I/C, M.D.S (Prosthodontics).",
        slno: "8",
        to: "30-06-2008"
      },
      {
        from: "30-06-2008",
        name: "Sri. S. N. NAGARAJ, K.A.S, I/C.",
        slno: "9",
        to: "06-09-2008"
      },
      {
        from: "06-09-2008",
        name:
          "Dr. ASHOK M. SHENOY, Addl. Charge FRCS(Ed) M.S (AIIMS) UICC (FELLOW).",
        slno: "10",
        to: "14-08-2009"
      },
      {
        from: "14-08-2009",
        name:
          "Dr. VIJAYA KUMAR, M.B.B.S, DNB (Gen. Surgery) FRCS (Glas) M.ch (once), PGDHM.",
        slno: "11",
        to: "27-12-2011"
      },
      {
        from: "28-12-2011",
        name:
          "Dr. VIJAYA KUMAR, M.B.B.S, DNB(Gen. Surgery) FRCS (Glas) M.ch (once), PGDHM",
        slno: "12",
        to: "29-12-2014"
      },
      {
        from: "29-12-2014",
        name: "Dr. L.APPAJI, I/C MD (pead),DM",
        slno: "13",
        to: "20-01-2015"
      },
      {
        from: "20-01-2015",
        name: "Dr. K.B. LINGEGOWDA,MD, FIPM (Palliative Medicine)",
        slno: "14",
        to: "28-08-2018"
      },
      {
        from: "28-08-2018",
        name: "Dr. RAMACHANDRA,M.B.B.S,M.S,M.ch",
        slno: "15",
        to: "continue"
      },
      {
        from: "12-11-2007",
        name: "Dr. RAMACHANDRA,M.B.B.S,M.S,M.ch",
        slno: "16",
        to: "30-4-2008"
      }
    ];
  }
}
