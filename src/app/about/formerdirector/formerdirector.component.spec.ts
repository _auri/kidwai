import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormerdirectorComponent } from './formerdirector.component';

describe('FormerdirectorComponent', () => {
  let component: FormerdirectorComponent;
  let fixture: ComponentFixture<FormerdirectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormerdirectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormerdirectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
