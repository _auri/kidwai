import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministrativeOfficersComponent } from './administrative-officers.component';

describe('AdministrativeOfficersComponent', () => {
  let component: AdministrativeOfficersComponent;
  let fixture: ComponentFixture<AdministrativeOfficersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdministrativeOfficersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministrativeOfficersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
