import { Component, OnInit } from "@angular/core";
import { FormerSup } from "../superintendents/superintendents.component";

@Component({
  selector: "app-administrative-officers",
  templateUrl: "./administrative-officers.component.html",
  styleUrls: ["./administrative-officers.component.scss"]
})
export class AdministrativeOfficersComponent implements OnInit {
  constructor() {}
  formerSup: FormerSup[];

  ngOnInit() {
    this.formerSup = [
      {
        from: "3-8-1988",
        name: "Sri. K.T Thimma Reddy, K.A.S ",
        slno: "1",
        to: "3-1-1991 "
      },
      {
        from: "10-1-1991",
        name: "Sri. B. Nanjundappa, K.A.S ",
        slno: "2",
        to: "25-3-1991"
      },
      {
        from: "25-3-1991",
        name: "Sri. B.L Mahalingappa, K.A.S",
        slno: "3",
        to: "13-1-1993 "
      },
      {
        from: "14-6-1993",
        name: "Sri. D. K Ranga Swamy, K.A.S",
        slno: "4",
        to: "24-10-1994"
      },
      {
        from: "31-10-1994",
        name: "Sri. Narayana Swamy, K.A.S",
        slno: "5",
        to: "31-5-1995 "
      },
      {
        from: "12-6-1995",
        name: "Sri. K. Narasimhayya,  K.A.S",
        slno: "6",
        to: "18-9-1995 "
      },
      {
        from: "29-9-1995",
        name: "Sri. G Ramachandra, K.A.S",
        slno: "7",
        to: "18-3-1996"
      },
      {
        from: "11-6-1996",
        name: "Sri. K.T Sudarshan, K.A.S",
        slno: "8",
        to: "31-3-1999"
      },
      {
        from: "10-5-1999 ",
        name: "Sri. N.M. Nilakantha, K.A.S",
        slno: "9",
        to: "15-10-1999"
      },
      {
        from: "3-12-1999",
        name: "Sri. H. Murthy, K.A.S",
        slno: "10",
        to: "19-1-2002"
      },
      {
        from: "19-1-2002",
        name: "Sri. N.G Nanja Raj, K.A.S",
        slno: "11",
        to: "4-8-2003"
      },
      {
        from: "11-9-2003",
        name: "Sri. Harif Ulla Shareef, K.A.S ",
        slno: "12",
        to: "31-12-2003"
      },
      {
        from: "26-11-2004",
        name: "Sri. KudriMothi, K.A.S ",
        slno: "13",
        to: "31-1-2005"
      },
      {
        from: "01-4-2005",
        name: "Sri. A. Mahendra, K.A.S",
        slno: "14",
        to: "30-9-2007"
      },
      {
        from: "12-11-2007",
        name: "Sri. B. Surendranath, K.A.S ",
        slno: "15",
        to: "30-4-2008"
      }
    ];
  }
}
