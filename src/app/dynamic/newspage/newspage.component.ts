import { Component, OnInit } from '@angular/core';
import { NavSubData } from 'src/app/data/nav-data.nav';

@Component({
  selector: 'app-newspage',
  templateUrl: './newspage.component.html',
  styleUrls: ['./newspage.component.scss']
})
export class NewspageComponent implements OnInit {
  data:NavSubData[]= [{
    menu: "News Release",
    submenu: []
  },
  {
    menu: "Newsletters",
    submenu: []
    },{
      menu: "Events",
      submenu: []
    },{
    menu: "COrders",
    submenu: [
      ]
    },
    {
      menu: "Circulars",
      submenu:[]
    },
    {
      menu: "Infographics",
      submenu:[]
    }
  ]
  constructor() { }

  ngOnInit() {
  }

}
