import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-newsupload',
  templateUrl: './newsupload.component.html',
  styleUrls: ['./newsupload.component.scss']
})
export class NewsuploadComponent implements OnInit {
  imageurl ='/assets/imagedummy.png'
  constructor() { }
  @ViewChild('fileUpload') fileUpload: ElementRef;
  ngOnInit() {
  }
  openFileSelector() {
    this.fileUpload.nativeElement.click();
    console.log('clicked');
  }
  fileSelected(event: any) {
    // this.storeImage = event.target.files[0];
    const fileReader: FileReader = new FileReader();
    fileReader.onload = (e: any) => {
      this.imageurl = e.target.result;
      console.log(this.imageurl);
      
    };
    // fileReader.readAsDataURL(this.storeImage);
    // console.log(this.storeImage);
  }
}
