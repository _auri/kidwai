import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsuploadComponent } from './newsupload.component';

describe('NewsuploadComponent', () => {
  let component: NewsuploadComponent;
  let fixture: ComponentFixture<NewsuploadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsuploadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsuploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
