export interface NavData {
    head: NavSubData[];

}
export interface NavSubData{
    menu: string,
    submenu:string[]
}
