import { Component, ElementRef, ViewChild, HostListener } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  dropdown = false;
  title = 'kidwai-website';
  coll: boolean = false;
  @HostListener('window:scroll', ['$event'])
  onWindowScroll(e) {
    if (window.pageYOffset > 130) {
      let element = document.getElementById('ftco-navbar');
      element.classList.add('sticky');
    } else {
     let element = document.getElementById('ftco-navbar');
       element.classList.remove('sticky'); 
    }
 }
}