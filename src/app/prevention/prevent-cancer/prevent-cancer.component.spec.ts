import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreventCancerComponent } from './prevent-cancer.component';

describe('PreventCancerComponent', () => {
  let component: PreventCancerComponent;
  let fixture: ComponentFixture<PreventCancerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreventCancerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreventCancerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
