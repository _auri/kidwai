import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CancerawarenessprogrammeComponent } from './cancerawarenessprogramme.component';

describe('CancerawarenessprogrammeComponent', () => {
  let component: CancerawarenessprogrammeComponent;
  let fixture: ComponentFixture<CancerawarenessprogrammeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CancerawarenessprogrammeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CancerawarenessprogrammeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
