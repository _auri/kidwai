import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignsandSymptomsComponent } from './signsand-symptoms.component';

describe('SignsandSymptomsComponent', () => {
  let component: SignsandSymptomsComponent;
  let fixture: ComponentFixture<SignsandSymptomsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignsandSymptomsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignsandSymptomsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
