import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CancerScreeningComponent } from './cancer-screening.component';

describe('CancerScreeningComponent', () => {
  let component: CancerScreeningComponent;
  let fixture: ComponentFixture<CancerScreeningComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CancerScreeningComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CancerScreeningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
