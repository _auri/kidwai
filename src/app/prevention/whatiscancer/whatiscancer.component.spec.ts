import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhatiscancerComponent } from './whatiscancer.component';

describe('WhatiscancerComponent', () => {
  let component: WhatiscancerComponent;
  let fixture: ComponentFixture<WhatiscancerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhatiscancerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhatiscancerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
