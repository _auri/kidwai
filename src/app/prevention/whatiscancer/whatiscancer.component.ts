import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-whatiscancer',
  templateUrl: './whatiscancer.component.html',
  styleUrls: ['./whatiscancer.component.scss']
})
export class WhatiscancerComponent implements OnInit {

  constructor() { }
  symptoms;
  ngOnInit() {
    this.symptoms = ["A mouth or tongue ulcer that lasts longer than 3 weeks",
      "A cough or hoarseness of voice that lasts longer than 3 weeks    ",
      "Difficulty in swallowing    ", "Frequent Indigestion", "Breathlessness",
      "Breathlessness", "Coughing up blood", "Changes in the size, shape or feel of the breasts"
      , "Any lump, dimpling or redness of the skin on the breasts", " Changes in the position of the nipple, a rash or nipple discharge"
      , "Bleeding from the vagina after sex, between periods or after the menopause",
      "Irregular or poor bowel movement /blood in stools /black stools", "Problems in urination like pain, blood, burning sensation"
      , "A change in the size, shape or colour of a mole or wart", "Repeated unexplained pain anywhere in the body"
      , "An unusual lump or swelling anywhere on the body", "Unexplained weight loss or heavy night sweats"
      , "A sore that does not heal even after several weeks","Unexplained fever for long period."
      
    
    ]
  }

}
