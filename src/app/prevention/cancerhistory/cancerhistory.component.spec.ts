import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CancerhistoryComponent } from './cancerhistory.component';

describe('CancerhistoryComponent', () => {
  let component: CancerhistoryComponent;
  let fixture: ComponentFixture<CancerhistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CancerhistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CancerhistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
