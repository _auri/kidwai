import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestCancerScreeningComponent } from './request-cancer-screening.component';

describe('RequestCancerScreeningComponent', () => {
  let component: RequestCancerScreeningComponent;
  let fixture: ComponentFixture<RequestCancerScreeningComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestCancerScreeningComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestCancerScreeningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
