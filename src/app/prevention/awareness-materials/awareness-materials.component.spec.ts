import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AwarenessMaterialsComponent } from './awareness-materials.component';

describe('AwarenessMaterialsComponent', () => {
  let component: AwarenessMaterialsComponent;
  let fixture: ComponentFixture<AwarenessMaterialsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AwarenessMaterialsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AwarenessMaterialsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
