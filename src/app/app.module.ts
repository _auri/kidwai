import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomePageComponent } from './common/home-page/home-page.component';
import { LeftnavComponent } from './common/leftnav/leftnav.component';
import { SlideshowModule } from 'ng-simple-slideshow';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SamplePageComponent } from './common/sample-page/sample-page.component';
import { NewspageComponent } from './dynamic/newspage/newspage.component';
import { NewscardsComponent } from './dynamic/newscards/newscards.component';
import { NewsuploadComponent } from './dynamic/newsupload/newsupload.component';
import { HistoryComponent } from './about/history/history.component';
import { IntroductionComponent } from './about/introduction/introduction.component';
import { FormerdirectorComponent } from './about/formerdirector/formerdirector.component';
import { SuperintendentsComponent } from './about/superintendents/superintendents.component';
import { AdministrativeOfficersComponent } from './about/administrative-officers/administrative-officers.component';
import { FinancialAdvisersComponent } from './about/financial-advisers/financial-advisers.component';
import { AchievementsComponent } from './about/achievements/achievements.component';
import { AffiliationsComponent } from './about/affiliations/affiliations.component';
import { DirectordeskComponent } from './about/directordesk/directordesk.component';
import { SuperintendentdeskComponent } from './about/superintendentdesk/superintendentdesk.component';
import { OrganizationChartComponent } from './about/organization-chart/organization-chart.component';
import { CommittessComponent } from './about/committess/committess.component';
import { StatisticsComponent } from './about/statistics/statistics.component';
import { AddressComponent } from './about/address/address.component';
import { PhoneComponent } from './about/phone/phone.component';
import { GooglelocationComponent } from './about/googlelocation/googlelocation.component';
import { HowtoreachComponent } from './about/howtoreach/howtoreach.component';
import { EnquiryComponent } from './about/enquiry/enquiry.component';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { PreventionComponent } from './common/prevention/prevention.component';
import { WhatiscancerComponent } from './prevention/whatiscancer/whatiscancer.component';
import { CancerhistoryComponent } from './prevention/cancerhistory/cancerhistory.component';
import { RiskfactorsComponent } from './prevention/riskfactors/riskfactors.component';
import { ProtectiveFactorComponent } from './prevention/protective-factor/protective-factor.component';
import { SignsandSymptomsComponent } from './prevention/signsand-symptoms/signsand-symptoms.component';
import { PreventCancerComponent } from './prevention/prevent-cancer/prevent-cancer.component';
import { CancerScreeningComponent } from './prevention/cancer-screening/cancer-screening.component';
import { CancerawarenessprogrammeComponent } from './prevention/cancerawarenessprogramme/cancerawarenessprogramme.component';
import { RequestCancerScreeningComponent } from './prevention/request-cancer-screening/request-cancer-screening.component';
import { AwarenessMaterialsComponent } from './prevention/awareness-materials/awareness-materials.component';
import { DownloadMaterialsComponent } from './prevention/download-materials/download-materials.component';
import { FormerdeansComponent } from './about/formerdeans/formerdeans.component';
import { MissionComponent } from './about/mission/mission.component';
import { PagenotfoundComponent } from './common/pagenotfound/pagenotfound.component';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { PatientcareComponent } from './common/patientcare/patientcare.component';
import { DepartmentComponent } from './common/department/department.component';
import { AcademicsComponent } from './common/academics/academics.component';
import { ResearchComponent } from './common/research/research.component';
import { StatisticsMenuComponent } from './common/statistics-menu/statistics-menu.component';
import { FormsModule } from "@angular/forms";
@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    LeftnavComponent,
    SamplePageComponent,
    NewspageComponent,
    NewscardsComponent,
    NewsuploadComponent,
    HistoryComponent,
    IntroductionComponent,
    FormerdirectorComponent,
    SuperintendentsComponent,
    AdministrativeOfficersComponent,
    FinancialAdvisersComponent,
    AchievementsComponent,
    AffiliationsComponent,
    DirectordeskComponent,
    SuperintendentdeskComponent,
    OrganizationChartComponent,
    CommittessComponent,
    StatisticsComponent,
    AddressComponent,
    PhoneComponent,
    GooglelocationComponent,
    HowtoreachComponent,
    EnquiryComponent,
    PreventionComponent,
    WhatiscancerComponent,
    CancerhistoryComponent,
    RiskfactorsComponent,
    ProtectiveFactorComponent,
    SignsandSymptomsComponent,
    PreventCancerComponent,
    CancerScreeningComponent,
    CancerawarenessprogrammeComponent,
    RequestCancerScreeningComponent,
    AwarenessMaterialsComponent,
    DownloadMaterialsComponent,
    FormerdeansComponent,
    MissionComponent,
    PagenotfoundComponent,
    PatientcareComponent,
    DepartmentComponent,
    AcademicsComponent,
    ResearchComponent,
    StatisticsMenuComponent
  ],
  imports: [
    BrowserModule,
    FormsModule ,
    AppRoutingModule, SlideshowModule,
    BrowserAnimationsModule,
    ScrollToModule.forRoot(),
    CarouselModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
