import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePageComponent } from "./common/home-page/home-page.component";
import { SamplePageComponent } from "./common/sample-page/sample-page.component";
import { NewspageComponent } from "./dynamic/newspage/newspage.component";
import { NewsuploadComponent } from "./dynamic/newsupload/newsupload.component";
import { PreventionComponent } from "./common/prevention/prevention.component";
import { DepartmentComponent } from "./common/department/department.component";
import { PatientcareComponent } from "./common/patientcare/patientcare.component";
import { ResearchComponent } from "./common/research/research.component";
import { AcademicsComponent } from "./common/academics/academics.component";
import {StatisticsMenuComponent} from "./common/statistics-menu/statistics-menu.component"
import { from } from 'rxjs';
const routes: Routes = [
  { path: "", redirectTo: "home", pathMatch: "full" },
  { path: "home", component: HomePageComponent },
  {
    path :"aboutus",component:SamplePageComponent
  },{
    path :"prevention",component:PreventionComponent
  },
  { path: 'department', component: DepartmentComponent },
  { path: 'patientcare', component: PatientcareComponent },
  { path: 'research', component: ResearchComponent },
  { path: 'academics', component: AcademicsComponent },
  { path: 'statistics', component: StatisticsMenuComponent },
  {
    path: "news", children: [
      { path: "", component: NewspageComponent },
      { path: "upload",component:NewsuploadComponent}
    ]
    
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
