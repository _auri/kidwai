import { Component, OnInit } from "@angular/core";
import { NavSubData } from "src/app/data/nav-data.nav";

@Component({
  selector: "app-prevention",
  templateUrl: "./prevention.component.html",
  styleUrls: ["./prevention.component.scss"]
})
export class PreventionComponent implements OnInit {
  page;
  data: NavSubData[] = [
    {
      menu: "What is Cancer",
      submenu: []
    },
    {
      menu: "History of Cancer",
      submenu: []
    },
    {
      menu: "Risk factors for Causes for Cancer",
      submenu: []
    },
    {
      menu: "Protective factors for Cancer",
      submenu: []
    },
    {
      menu: "Signs and Symptoms of Cancer",
      submenu: []
    },{
      menu: "How to Prevent Cancer",
      submenu:[]
    }, {
      menu: "Cancer Screening",
      submenu:["Breast Cancer","Cervical Cancer","Oral Cancer"]
    }, {
      menu: "Online request for Cancer awareness programme",
      submenu:[]
    }, {
      menu: "Online request for Cancer Screening",
      submenu:[]
    }, {
      menu: "Cancer awareness and Screening feed back",
      submenu:[]
    }
    , {
      menu: "Donation for Cancer Screening and Awarenes programme",
      submenu:[]
    },
    {
      menu: "Download Cancer Awareness Materials",
      submenu:[]
    }
  ];

  constructor() {}

  ngOnInit() { }
  displayCounter(event) {
    
  }
}
