import { Component, OnInit } from "@angular/core";
import { NavData, NavSubData } from "src/app/data/nav-data.nav";
import {
  ScrollToService,
  ScrollToConfigOptions
} from "@nicky-lenaers/ngx-scroll-to";

@Component({
  selector: "app-sample-page",
  templateUrl: "./sample-page.component.html",
  styleUrls: ["./sample-page.component.scss"]
})
export class SamplePageComponent implements OnInit {
  about: NavSubData[];
  page;
  data: NavSubData[] = [
    {
      menu: "About KMIO",
      submenu: [
        " Introduction",
        "History",
        "Former Deans",
        "Former Directors",
        "Former Medical Superintendents",
        "Former Chief Administrative Officers",
        "Former Financial Advisers",
        "Mission, Vision & Values",
        "Awards & Achievements",
        "Affiliations"
      ]
    },
    {
      menu: "Governance",
      submenu: [" Director’s Desk", "Organization Chart ", "Committees "]
    },
    {
      menu: "Departmental Staffs",
      submenu: []
    },
    {
      menu: "Statistics",
      submenu: []
    },
    {
      menu: "Contact Us",
      submenu: ["Address", "Phone", "How to Reach Kidwai", "Enquiry"]
    }
  ];
  aboutus: boolean = true;
  governance: boolean = false;
  stati: boolean = false;
  contactus: boolean = false;
  enquiry: boolean = false;
  constructor(private _scrollToService: ScrollToService) {
    console.log(this.about);
  }
  displayCounter(count) {
    this.page = count;
    console.log(count);

    switch (count) {
      case "About KMIO":
        this.aboutus = true;
        this.governance = false;
        this.stati = false;
        this.contactus = false;
        this.enquiry = false;
        break;
      case " Introduction":
        const config: ScrollToConfigOptions = {
          offset: 150,
          target: "introduction"
        };
        this._scrollToService.scrollTo(config);
        break;
      case "Former Deans":
        const config12: ScrollToConfigOptions = {
          offset: 200,
          target: "formerdeans"
        };
        this._scrollToService.scrollTo(config12);
        break;
      case "History":
        const config1: ScrollToConfigOptions = {
          offset: 200,
          target: "history"
        };
        this._scrollToService.scrollTo(config1);
        break;
      case "Former Directors":
        const config2: ScrollToConfigOptions = {
          offset: 200,
          target: "formerdirector"
        };
        this._scrollToService.scrollTo(config2);
        break;
      case "Former Medical Superintendents":
        const config3: ScrollToConfigOptions = {
          offset: 200,
          target: "superintendents"
        };
        this._scrollToService.scrollTo(config3);
        break;
      case "Former Chief Administrative Officers":
        const config4: ScrollToConfigOptions = {
          offset: 200,

          target: "administrative-officers"
        };
        this._scrollToService.scrollTo(config4);
        break;
      case "Former Financial Advisers":
       
        const config18: ScrollToConfigOptions = {
          offset: 200,

          target: "financial-advisers"
        };
        this._scrollToService.scrollTo(config18);
        break;
      case "Awards & Achievements":
        const config5: ScrollToConfigOptions = {
          offset: 200,
          target: "achievements"
        };
        this._scrollToService.scrollTo(config5);
        break;
      case "Affiliations":
        const config6: ScrollToConfigOptions = {
          offset: 200,
          target: "affiliations"
        };
        this._scrollToService.scrollTo(config6);
        break;
      case "Mission, Vision & Values":
        const config7: ScrollToConfigOptions = {
          offset: 200,
          target: "mission"
        };
        this.aboutus = true;
        this._scrollToService.scrollTo(config7);
        break;
      case " Director’s Desk":
        const config8: ScrollToConfigOptions = {
          offset: 150,
          target: "directordesk"
        };
        this.aboutus = false;
        this.governance = true;
        this.stati = false;
        this.contactus = false;
        this.enquiry = false;
        this._scrollToService.scrollTo(config8);
        break;
      case "Organization Chart ":
        const config9: ScrollToConfigOptions = {
          offset: 200,
          target: "organization"
        };
        this.aboutus = false;
        this.governance = true;
        this.stati = false;
        this.contactus = false;
        this.enquiry = false;
        this._scrollToService.scrollTo(config9);
        break;
      case "Committees ":
        const config10: ScrollToConfigOptions = {
          offset: 200,
          target: "committess"
        };
        this.aboutus = false;
        this.governance = true;
        this.stati = false;
        this.contactus = false;
        this.enquiry = false;
        this._scrollToService.scrollTo(config10);
        break;
      case "Departmental Staffs":
      this.stati = true;
      this.governance = false;
      this.aboutus = false;
      this.contactus = false;
      this.enquiry = false;
        break;
      case "Address":
        const config11: ScrollToConfigOptions = {
          offset: 150,
          target: "address"
        };
        this.stati = false;
        this.governance = false;
        this.aboutus = false;
        this.contactus = true;
        this.enquiry = false;
        this._scrollToService.scrollTo(config11);
        break;

      case "Phone":
        const config13: ScrollToConfigOptions = {
          offset: 150,
          target: "phone"
        };
        this.stati = false;
        this.governance = false;
        this.aboutus = false;
        this.contactus = true;
        this.enquiry = false;
        this._scrollToService.scrollTo(config13);
        break;
      case "How to Reach Kidwai":
        const config15: ScrollToConfigOptions = {
          offset: 200,
          target: "howtoreach"
        };
        this.stati = false;
        this.governance = false;
        this.aboutus = false;
        this.contactus = true;
        this.enquiry = false;
        this._scrollToService.scrollTo(config15);
        break;
      case "Governance":
        this.aboutus = false;
        this.governance = true;
        this.stati = false;
        this.contactus = false;
        this.enquiry = false;
        const config17: ScrollToConfigOptions = {
          target: "top"
        };
        this._scrollToService.scrollTo(config17);
        break;
      case "Statistics":
        this.stati = true;
        this.governance = false;
        this.aboutus = false;
        this.contactus = false;
        this.enquiry = false;

        break;
      case "Address":
        this.stati = false;
        this.governance = false;
        this.aboutus = false;
        this.contactus = true;
        this.enquiry = false;
        break;
      case "Phone, Email, Fax":
        this.stati = false;
        this.governance = false;
        this.aboutus = false;
        this.contactus = true;
        this.enquiry = false;

        break;
      case "Google Location":
        this.stati = false;
        this.governance = false;
        this.aboutus = false;
        this.contactus = true;
        this.enquiry = false;
        break;
      case "Contact Us":
        this.stati = false;
        this.governance = false;
        this.aboutus = false;
        this.contactus = true;
        this.enquiry = false;
        break;
      case "Enquiry":
        this.stati = false;
        this.governance = false;
        this.aboutus = false;
        this.contactus = false;
        this.enquiry = true;
        const config16: ScrollToConfigOptions = {
          target: "top"
        };
        this._scrollToService.scrollTo(config16);
        break;
      default:
        break;
    }
  }

  ngOnInit() {
    const config1: ScrollToConfigOptions = {
      target: "top"
    };
    this._scrollToService.scrollTo(config1);
  }
}
