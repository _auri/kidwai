import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NavData,NavSubData } from 'src/app/data/nav-data.nav';

@Component({
  selector: 'app-leftnav',
  templateUrl: './leftnav.component.html',
  styleUrls: ['./leftnav.component.scss']
})
  
export class LeftnavComponent implements OnInit {
  @Input() navData: NavSubData;
  @Output() valueChange = new EventEmitter();
  constructor() {

  }

  ngOnInit() {

  }
  onClicedMenu(nav:NavSubData) {
    // console.log(nav.submenu.length);
    // if (nav.submenu.length == 0) {
      this.valueChange.emit(nav.menu);
    // }
    
  }
  onClickedSubMenu(subMenu) {
    // console.log(subMenu);
    this.valueChange.emit(subMenu);
  }
 
}
