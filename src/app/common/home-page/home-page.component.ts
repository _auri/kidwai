import { Component, OnInit } from "@angular/core";
import { timer } from 'rxjs';
import { trigger, transition, animate, style } from '@angular/animations'

@Component({
  selector: "app-home-page",
  templateUrl: "./home-page.component.html",
  styleUrls: ["./home-page.component.scss"],
  animations: [
    trigger('slideInOut', [
      transition(':enter', [
        style({transform: 'translateX(-100%)'}),
        animate('2000ms ease-in', style({transform: 'translateX(0%)'}))
      ]),
      // transition(':leave', [
      //   animate('2000ms ease-in', style({transform: 'translateX(-100%)'}))
      // ])
    ])
  ]
})
export class HomePageComponent implements OnInit {
  image = "../../assets/bg_1.jpg";
  image1 = "../../assets/bg_01.jpg";
  transform = 50;
  interval;
  constructor() {}
  imageSources = [
  
    "../../assets/bg_7.JPG",
    "../../assets/bg_3.jpg",
    "../../assets/bg_4.jpg",
    
    
  ];
  customOptions: any = {
    loop: false,
    dragging: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: false,
    dots: true,
    navSpeed: 700,
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 1
      },
      740: {
        items: 2
      },
      940: {
        items: 3
      }
    },
    nav: false
  }
  ex: boolean = false;

  ngOnInit() {
 
  }
}
